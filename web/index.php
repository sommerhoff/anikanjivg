<?php

// AniKanjiVG X is the web interface for AniKanjiVG, by Paul C. Sommerhoff
// The animated SVG files from AniKanjiVG must be stored in the sub-folder "svg".

function redirect($url, $statusCode = 303)
{
   header('Location: ' . $url, true, $statusCode);
   die();
}

$example = "食";
$mysqli = new mysqli('host', 'anikanjivgx', 'password', 'anikanjivgx');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

$mysqli->query("SET NAMES 'utf8'");

// API: get raw SVG data if parameter "svg" is specified
$svg = htmlspecialchars($_GET['svg']);

// 1 kanji has length 3
if(strlen($svg) == 3) {
    if ($result = $mysqli->query("SELECT * FROM kanjivg WHERE kanji LIKE '".$svg."'")) {
	while ($row = $result->fetch_row()) {
	    redirect("svg/$row[0].svg");
	}
	$result->close();
    }
}

// get secure search string from GET request
$search = htmlspecialchars($_GET['search']);
?>
<html>
  <head>
    <title>AniKanjiVG X</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta charset="utf-8">
  </head>
  
  <body>

    <center>
    
      <h1>AniKanjiVG X</h1>
      
      <br>
      
      <div>Search for kanji animation (single kanji only)</div>
      <form method="get">
        <input type="text" name="search" value="<?=$search ?>" maxlength="1" size="4" style="font-size: 24px;padding-bottom:6px;">
        <button type="submit" class="btn btn-success" style="font-size: 20px;">Search</button>
      </form>
      <br>
      <br>

<?php
if(!empty($search)) {
    if(strlen($search) == 3) {
	if ($result = $mysqli->query("SELECT * FROM kanjivg WHERE kanji LIKE '".$search."'")) {
	    $found = false;
	    while ($row = $result->fetch_row()) {
		echo "      <div>Here is the animation for kanji ".$row[1].':<div><img src="svg/'.$row[0].'.svg"></div></div>';
		$example = $search;
		$found = true;
	    }
	    $result->close();
	    if(!$found) {
	        echo "      <div>Character not found.</div>";
	    }
	}
    } else {
        echo "      <div>Please search only a single kanji!</div>";
    }
}
$mysqli->close();
?>
      <br>
      
      <div>We recommend the Chromium browser to enjoy nice animations.</div>
      
      <br>
      
      <div>You can also use the &quot;svg&quot; parameter as API to get to the SVG file right away. Try
      <a href="http://example.com/anikanjivgx/?svg=<?=$example ?>">http://example.com/anikanjivgx/?svg=<?=$example ?></a>.</div>
      
      <br>
      <br>

      <div>
      This project uses Creative Commons licenses. See image link for information about the used licenses.
      <br>
      <a href="https://creativecommons.org/licenses/by-sa/3.0/"><img src="https://licensebuttons.net/l/by-sa/3.0/88x31.png" /></a>
      <br>
      <a href="https://bitbucket.org/sommerhoff/anikanjivg">AniKanjiVG</a> is licensed by CC BY-SA Paul C. Sommerhoff, <a href="http://kanjivg.tagaini.net/">KanjiVG</a> is licensed by CC BY-SA Ulrich Apel.
      </div>

      <br>
      <br>
      
      <div><a href="http://example.com">Legal notes</a></div>
      
    </center>

  </body>
</html>
