# README #

AniKanjiVG provides animated kanjis in SVG format.
The animated kanjis can be found as zip archive in the folder "releases".
So AniKanjiVG helps towards visualizing the stroke order of kanjis.

For example, the Chromium browser provides pretty good SVG animations for this project.

There is also a small website, so that the animations can be viewed in action at https://www.miraisoft.de/anikanjivgx/

The website code for AniKanjiVG X can be found in this repository under web/index.php.

Programmed by: Paul C. Sommerhoff, 2016

AniKanjiVG is licensed by CC BY-SA Paul C. Sommerhoff (https://bitbucket.org/sommerhoff/anikanjivg).
AniKanjiVG uses KanjiVG which is licensed by CC BY-SA Ulrich Apel (http://kanjivg.tagaini.net).
See also http://creativecommons.org/licenses/by-sa/3.0/