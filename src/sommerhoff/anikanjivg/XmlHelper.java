package sommerhoff.anikanjivg;

import org.xml.sax.Attributes;

/**
 * Small XML helper class
 * @author Paul C. Sommerhoff, 2016
 *
 */
public class XmlHelper {

	public static String createXml(String elementName, Attributes attributes) {
		StringBuffer xml = new StringBuffer();
		xml.append("<" + elementName);
		for(int i=0; i<attributes.getLength(); i++) {
			xml.append(" " + attributes.getQName(i) + "=\"" + attributes.getValue(i) + "\"");
		}
		xml.append(">");
		return xml.toString();
	}
	
	public static String createXml(String elementName, Attributes attributes, String additional) {
		StringBuffer xml = new StringBuffer();
		xml.append("<" + elementName + " " + additional);
		for(int i=0; i<attributes.getLength(); i++) {
			xml.append(" " + attributes.getQName(i) + "=\"" + attributes.getValue(i) + "\"");
		}
		xml.append(">");
		return xml.toString();
	}
}
