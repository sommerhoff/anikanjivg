package sommerhoff.anikanjivg;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Parses the KanjiVG XML file and generates animated kanjis in SVG format.
 * Also stores KanjiVG data in database table (necessary for AniKanjiVG X website, see web/index.php).
 * @author Paul C. Sommerhoff, 2016
 *
 */

public class Parser extends DefaultHandler implements Runnable {
	private final static String RESOURCE_KANJIVG = "./resources/kanjivg.xml";
	private final static String OUTPUT_FOLDER = "/tmp/";
	private final static String DB_HOST = "localhost";
	private final static String DB_USER = "anikanjivgx";
	private final static String DB_PASSWORD = "anikanjivgx";
	private final static String DB_DATABASE = "anikanjivgx";
	
	private Database db;
	private PreparedStatement stmtInsert;
	private List<String> kanjis;
	private boolean inNewKanji;
	
	private List<String> kanjisXml;
	private List<String> ids;
	private StringBuffer currentXmlKanji;
	private int currentPathNumber;
	
	@Override
	public void run() {
		File xmlFile = new File(RESOURCE_KANJIVG);
		File outputFolder = new File(OUTPUT_FOLDER);
		
		if(!xmlFile.exists()) {
			System.err.println("KanjiVG xml file does not exist in " + RESOURCE_KANJIVG);
			return;
		}
		if(!outputFolder.exists() || !outputFolder.isDirectory()) {
			System.err.println("Error: Output folder is set to " + OUTPUT_FOLDER);
			return;
		}
		
		db = new Database(DB_HOST, DB_DATABASE, DB_USER, DB_PASSWORD);
		db.connect();
		
		try {
			String tableSql = "CREATE TABLE IF NOT EXISTS `kanjivg` ("
				+ "`id` varchar(11) NOT NULL,"
				+ "`kanji` varchar(1) NOT NULL,"
				+ "PRIMARY KEY (`id`),"
				+ "KEY `kanji` (`kanji`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			PreparedStatement stmtCreate = db.getConnection().prepareStatement(tableSql);
			stmtCreate.execute();
			String truncateSql = "TRUNCATE `kanjivg`";
			PreparedStatement stmtTruncate = db.getConnection().prepareStatement(truncateSql);
			stmtTruncate.execute();
			String insertSql = "INSERT INTO kanjivg VALUES (?,?)";
			stmtInsert = db.getConnection().prepareStatement(insertSql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(xmlFile, this);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		
		for(int i=0; i<kanjisXml.size(); i++) {
			try {
				FileWriter writer = new FileWriter(OUTPUT_FOLDER + ids.get(i) + ".svg");
				writer.write(kanjisXml.get(i));
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				// TODO what if kanji length != 1?
				if(ids.get(i) != null && kanjis.get(i) != null && kanjis.get(i).length() == 1) {
					stmtInsert.setString(1, ids.get(i));
					stmtInsert.setString(2, kanjis.get(i));
					stmtInsert.addBatch();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		try {
			stmtInsert.executeBatch();
			db.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Parsing KanjiVG and writing animated SVGs into "+ OUTPUT_FOLDER +" finished.");
		
		/*
		int random = (int) (Math.random() * kanjisXml.size());
		System.out.println("Example: Kanji " + random);
        System.out.println(kanjisXml.get(random));
		*/
	}
	
	@Override
	public void startDocument() throws SAXException {
		kanjisXml = new ArrayList<>();
		ids = new ArrayList<>();
		kanjis = new ArrayList<>();
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch(qName) {
		case "kanjivg":
			break;
		case "kanji":
			currentPathNumber = 0;
			currentXmlKanji = new StringBuffer();
			currentXmlKanji.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			currentXmlKanji.append("<!--\nAniKanjiVG is licensed by CC-BY-SA Paul C. Sommerhoff "
					+ "(https://bitbucket.org/sommerhoff/anikanjivg).\n"
					+ "AniKanjiVG uses KanjiVG which is licensed by CC-BY-SA Ulrich Apel "
					+ "(http://kanjivg.tagaini.net).\n"
					+ "See also http://creativecommons.org/licenses/by-sa/3.0/\n-->\n");
			currentXmlKanji.append(XmlHelper.createXml("svg", attributes, "xmlns:kvg='http://kanjivg.tagaini.net' xmlns='http://www.w3.org/2000/svg' "
					+ "width='109' height='109' viewBox='0 0 109 109' "
					+ "style='fill:none;stroke:#000000;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;'"));
			// id without prefix "kvg:kanji_"
			String id = attributes.getValue("id").substring(10);
			// look for duplicate kanjis (only 1 duplicate in XML file, so that's an anomaly)
			String tempId = id;
			int idNumber = 1;
			while(ids.contains(tempId)) {
				tempId = id + "_" + idNumber;
				idNumber++;
			}
			ids.add(tempId);
			inNewKanji = true;
			break;
		case "g":
			if(inNewKanji) {
				kanjis.add(attributes.getValue("kvg:element"));
				inNewKanji = false;
			}
			currentXmlKanji.append(XmlHelper.createXml("g", attributes));
			break;
		case "path":
			currentXmlKanji.append(XmlHelper.createXml("path", attributes));
			currentXmlKanji.append(SvgAnimate.getAnimationXML(currentPathNumber, attributes.getValue("d")));
			currentPathNumber++;
			break;
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch(qName) {
		case "kanji":
			currentXmlKanji.append("</svg>");
			kanjisXml.add(currentXmlKanji.toString());
			break;
		case "g":
			currentXmlKanji.append("</g>\n");
			break;
		case "path":
			currentXmlKanji.append("</path>\n");
			break;
		}
	}
	
}
