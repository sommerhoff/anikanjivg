package sommerhoff.anikanjivg;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * This is a basic MySQL database class.
 * 
 * @author Paul C. Sommerhoff
 *
 */
public class Database {
	protected Connection con;
	protected String url;
	protected String db;
	protected String user;
	protected String password;
	

	
	public Database(String url, String db, String user, String password) {
		con = null;
		this.url = url;
		this.db = db;
		this.user = user;
		this.password = password;
	}

	public void connect() {
		try {
			if(con == null) {
				Properties props = new Properties();
				props.put("user",user);
				props.put("password",password);
				props.put("useUnicode","true");
				props.put("characterEncoding","UTF-8");
				props.put("mysqlEncoding","utf-8");
	            Class.forName ("com.mysql.jdbc.Driver").newInstance ();
	            con = DriverManager.getConnection("jdbc:mysql://"+url+":3306/"+db+"?rewriteBatchedStatements=true", props);
	            System.out.println("Database connection established at server "+url);
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Critical connection error with server '"+url+"' and class '"+this.getClass().getSimpleName()+"'");
			System.out.println("Please check database settings in config and restart program!");
			System.exit(-1);
			
		}   
	}

	public void close() {
		try {
			con.close();
			con = null;
		} catch (SQLException e) {
			System.out.println("Closing error: "+e.getMessage());
		}
	}
	
	public void executeUpdate(String sql) {
		Statement s;
		try {
			s = con.createStatement();
			s.executeUpdate(sql);
			s.close();
		} catch (SQLException e) {
			System.out.println("SQL Update error: "+e.getMessage());
			System.exit(-1);
		}
	}
	
	public ResultSet executeQuery(String sql) {
		try {
			Statement s = con.createStatement ();
			s.executeQuery (sql);
			return s.getResultSet ();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Connection getConnection() {
		return con;
	}

}
