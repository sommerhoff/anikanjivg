package sommerhoff.anikanjivg;

import java.util.ArrayList;

/**
 * Provides the animations, more precisely the SVG "animate" elements.
 * @author Paul C. Sommerhoff, 2016
 *
 */
public class SvgAnimate {
	/** time to draw one path line */
	public final static double DRAW_TIME = 1.0;
		
	public static String getAnimationXML(int pathNumber, String pathValue) {
		StringBuffer xml = new StringBuffer();
		
		int nextIndexOfC = getNextIndexOfC(pathValue);
		String m = pathValue.substring(0, nextIndexOfC);
		String mPoint = pathValue.substring(1, nextIndexOfC);
		ArrayList<String> listOfC = new ArrayList<>();
		String remainingPath = pathValue;
		while(nextIndexOfC != -1) {
			remainingPath = remainingPath.substring(nextIndexOfC);
			nextIndexOfC = getNextIndexOfC(remainingPath);
			if(nextIndexOfC != -1) {
				listOfC.add(remainingPath.substring(0, nextIndexOfC));
			} else {
				listOfC.add(remainingPath);
			}
		}
		
		double time = DRAW_TIME / listOfC.size(); 
		
		xml.append("<animate ");
		xml.append("attributeName=\"d\" ");
		xml.append("begin=\"" + pathNumber * DRAW_TIME + "s\" ");
		xml.append("dur=\"" + time + "s\" ");
		xml.append("from=\"" + m + "C" + mPoint + ","+ mPoint + "," + mPoint + "\" ");
		xml.append("to=\"" + m + listOfC.get(0) + "\" ");
		xml.append("/>\n");
		
        for(int i=0; i<listOfC.size()-1; i++) {
        	String from = m;
        	String to = m;
        	for(int j=0; j<=i; j++) {
        		from = from + listOfC.get(j);
        	}
        	for(int j=0; j<=i+1; j++) {
        		to = to + listOfC.get(j);
        	}
        	from = from + "c0,0,0,0,0,0,0";
        	
        	xml.append("<animate ");
    		xml.append("attributeName=\"d\" ");
    		xml.append("begin=\"" + (pathNumber * DRAW_TIME + time * (i+1)) + "s\" ");
    		xml.append("dur=\"" + time + "s\" ");
    		xml.append("from=\"" + from + "\" ");
    		xml.append("to=\"" + to + "\" ");
    		xml.append("/>\n");
    		
        }
        
        // make stroke invisible till it is drawn
        if(pathNumber != 0) {
        	xml.append("<animate ");
    		xml.append("attributeName=\"d\" ");
    		xml.append("begin=\"0s\" ");
    		xml.append("dur=\"" + pathNumber * DRAW_TIME + "s\" ");
    		xml.append("from=\"M-100,-100\" ");
    		xml.append("to=\"M-100,-100\" ");
    		xml.append("/>\n");
        }

        return xml.toString();
        
	}
	
	private static int getNextIndexOfC(String path) {
		int end = -1;
		if(path.indexOf("c",1) != -1) {
			end = path.indexOf("c", 1);
		}
		if(path.indexOf("C", 1) != -1) {
			if(end == -1) {
				end = path.indexOf("C", 1);
			} else if(path.indexOf("C", 1) < end) {
				end = path.indexOf("C", 1);
			}
		}
		return end;
	}
	
}
