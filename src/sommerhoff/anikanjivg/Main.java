package sommerhoff.anikanjivg;

/**
 * Main class that starts the parser
 * @author Paul C. Sommerhoff, 2016
 *
 */
public class Main {

	public static void main(String[] args) {
		Parser p = new Parser();
		p.run();
	}
	
}
